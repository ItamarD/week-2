#include "Mitochondrion.h"
#include "AminoAcid.h"
#include "Nucleus.h"
#include "Protein.h"
#include "Ribosome.h"

void Mitochondrion::init()
{
	this->_glucuse_level = 0;
	this->_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	Protein temp = protein;
	AminoAcid acidNames[] = { ALANINE,LEUCINE,GLYCINE,HISTIDINE,LEUCINE,PHENYLALANINE,AMINO_CHAIN_END };
	AminoAcidNode* acid = new AminoAcidNode();
	acid = temp.get_first();
	AminoAcidNode* temp2 = acid;
	int i = 0, flag = 1;
	while (temp2 != NULL && flag)
	{
		if (temp2->get_data() != acidNames[i])
		{
			this->_has_glocuse_receptor = false;
			flag = 0;
		}
		temp2 = temp2->get_next();
		i++;
	}
	if (flag)
	{
		this->_has_glocuse_receptor = true;
	}
}

void Mitochondrion::set_glucose(const unsigned int glucose_units)
{
	this->_glucuse_level = glucose_units;
}

bool Mitochondrion::produceATP() const
{
	int flag = 0;
	if (this->_glucuse_level >= 50 && this->_has_glocuse_receptor)
	{
		flag = 1;
	}
	return flag;
}
