#include "Nucleus.h"
#include <iostream>

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_strand_dna_complementary_on = on_complementary_dna_strand;
}

unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_strand_dna_complementary_on;
}

void Gene::set_start(unsigned int _start)
{
	this->_start = _start;
}

void Gene::set_end(unsigned int _end)
{
	this->_end = _end;
}

void Gene::set_strand_dna_complementary_on(const bool _strand_dna_complementary_on)
{
	this->_strand_dna_complementary_on = _strand_dna_complementary_on;
}

void Nucleus::init(const std::string dna_sequence)
{
	int i = 0;
	this->_strand_DNA = dna_sequence;
	for (i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] == 'G')
		{
			this->_complementary_DNA_strand += 'C';
		}
		else if (dna_sequence[i] == 'C')
		{
			this->_complementary_DNA_strand += 'G';
		}
		else if (dna_sequence[i] == 'T')
		{
			this->_complementary_DNA_strand += 'A';
		}
		else if (dna_sequence[i] == 'A')
		{
			this->_complementary_DNA_strand += 'T';
		}
		else
		{
			std::cerr << "DNA invalid";
			_exit(1);
		}
	}

	
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	unsigned int start = gene.get_start();
	unsigned int end = gene.get_end();
	std::string temp = "";
	std::string temp2 = "";
	if (gene.is_on_complementary_dna_strand())
	{
		temp += this->_complementary_DNA_strand;
	}
	else
	{
		temp += this->_strand_DNA;
	}

	temp2 = temp.substr(start, end-start);
	for (int i = 0; i < temp2.length(); i++)
	{
		if (temp2[i] == 'T')
		{
			temp2[i] = 'U';
		}
	}


	return temp2;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string temp = _strand_DNA;
	std::reverse(temp.begin(), temp.end());
	return temp;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int occurrences = 0;
	std::string::size_type pos = 0;
	std::string s = _strand_DNA;
	std::string target = codon;
	while ((pos = s.find(target, pos)) != std::string::npos) 
	{
		++occurrences;
		pos += target.length();
	}
	return occurrences;
}
