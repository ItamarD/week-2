#pragma once
#include <string>
class Gene
{
private:
		unsigned int _start;
		unsigned int _end;
		bool _strand_dna_complementary_on;
public:
		void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
		unsigned int get_start() const;
		unsigned int get_end() const;
		bool is_on_complementary_dna_strand()const;
		void set_start(const unsigned int _start);
		void set_end(const unsigned int _end);
		void set_strand_dna_complementary_on(const bool _strand_dna_complementary_on);

};
class Nucleus
{
private:
	std::string _strand_DNA;
	std::string _complementary_DNA_strand;
public:
	void init(const std::string dna_sequence);
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;

};