#include "Cell.h"

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
}

bool Cell::get_ATP()
{
	std::string RNA = "";
	RNA = this->_nucleus.get_RNA_transcript(_glocus_receptor_gene);
	Protein* temp = this->_ribosome.create_protein(RNA);
	if (temp == nullptr)
	{
		std::cerr << "RNA error";
		_exit(1);
	}
	this->_mitochondrion.insert_glucose_receptor(*temp);
	this->_mitochondrion.set_glucose(50);
	bool atp = this->_mitochondrion.produceATP();
	if (!atp)
	{
		return false;
	}
	this->_atp_units = 100;
	return true;
}
