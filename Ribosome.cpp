#include "Ribosome.h"
#include "Protein.h"
#include "AminoAcid.h"
#include <string>

Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	Protein* new_protein = new Protein();
	int len = RNA_transcript.length();
	int i = 0, c = 0;
	AminoAcid acid = UNKNOWN;
	new_protein->init();
	std::string temp = "";
	while (RNA_transcript.length() >= 3)
	{
		temp = RNA_transcript.substr(0, 3);
		acid = get_amino_acid(temp);
		if (acid == UNKNOWN)
		{
			new_protein->clear();
			return nullptr;
		}
		else
		{
			RNA_transcript.erase(0, 3);
			new_protein->add(acid);
		}

	}
	return new_protein;
}